package ltu.shop.cart;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ltu.shop.product.ProductFromService;

@RestController
@Api(value = "cart products")
@RequestMapping(value = "/api/users/{username}/cart-products")
public class CartController {

	@Autowired
	private CartService cartService;

	public CartService getCartService() {
		return cartService;
	}

	public void setCartService(CartService cartService) {
		this.cartService = cartService;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get products", notes = "Returns all products")
	public List<ProductFromService> getProductsInCart(@PathVariable final String username) {

		return cartService.getProductsInCart(username);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Add product to cart", notes = "Adds product to user cart")
	public ResponseEntity<List<ProductFromService>> addToCart(@PathVariable final String username,
			@RequestBody final ProductFromService cmd) {
		Long id = cmd.getId();

		List<ProductFromService> currentProducts = cartService.addToCart(username, id);

		return new ResponseEntity<>(currentProducts, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete product", notes = "Removes product from user cart")
	public ResponseEntity<List<ProductFromService>> deleteFromCart(@PathVariable final String username,
			@PathVariable Long id) {
		List<ProductFromService> remainingProducts = cartService.deleteFromCart(username, id);
		return new ResponseEntity<>(remainingProducts, HttpStatus.OK);
	}

}
