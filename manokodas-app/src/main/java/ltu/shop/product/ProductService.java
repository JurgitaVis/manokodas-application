package ltu.shop.product;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService {
	@Autowired
	private ProductDAO productDao;

	public ProductDAO getProductDao() {
		return productDao;
	}

	public void setProductDao(ProductDAO productDao) {
		this.productDao = productDao;
	}

	@Transactional(readOnly = true)
	public List<ProductFromService> getProducts() {
		return productDao.findAll().stream()
				.map(product -> new ProductFromService(product.getId(), product.getTitle(), product.getType(),
						product.getDescription(), product.getImage(), product.getPrice(), product.getQuantity()))
				.collect(Collectors.toList());
	}

	@Transactional
	public void addProduct(ProductFromService product) {

		String title = product.getTitle();
		String type = product.getType();
		String description = product.getDescription();
		String image = product.getImage();
		BigDecimal price = product.getPrice();
		int quantity = product.getQuantity();

		Product productToSave = null;

		if (type.equals("Telefonas")) {
			productToSave = new Phone();
		} else {
			productToSave = new OtherProduct();
		}

		productToSave.setTitle(title);
		productToSave.setDescription(description);
		productToSave.setImage(image);
		productToSave.setPrice(price);
		productToSave.setQuantity(quantity);

		productDao.save(productToSave);
	}

	@Transactional
	public ProductFromService getProduct(Long id) {

		Product product = productDao.findById(id).orElse(null);
		if (product == null) {
			return new ProductFromService();
		}
		return new ProductFromService(product.getId(), product.getTitle(), product.getType(), product.getDescription(),
				product.getImage(), product.getPrice(), product.getQuantity());
	}

	@Transactional
	public List<ProductFromService> getProductsByTitle(String title) {
		List<Product> productsByTitle = productDao.findByTitleFragment(title);

		return productsByTitle.stream()
				.map(product -> new ProductFromService(product.getId(), product.getTitle(), product.getType(),
						product.getDescription(), product.getImage(), product.getPrice(), product.getQuantity()))
				.collect(Collectors.toList());
	}

	@Transactional
	public void deleteProduct(Long id) {
		productDao.deleteById(id);
	}

	@Transactional
	public void updateProduct(ProductFromService product) {
		Product productFromDb = productDao.findById(product.getId()).orElse(null);

		if (productFromDb != null) {
			productFromDb.setTitle(product.getTitle());
			productFromDb.setDescription(product.getDescription());
			productFromDb.setImage(product.getImage());
			productFromDb.setPrice(product.getPrice());
			productFromDb.setQuantity(product.getQuantity());

			productDao.save(productFromDb);
		}

	}

}
