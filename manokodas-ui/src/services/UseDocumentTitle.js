import { useEffect } from 'react';


export default function UseDocumentTitle(title) {
    useEffect(() => {
        document.title = title;

    });

}
