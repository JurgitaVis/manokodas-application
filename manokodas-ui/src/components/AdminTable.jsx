import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import itemImg from "../img/samsung-small.jpg";
import Table from '../common/Table'

class AdminTable extends Component {
    columns = [
        {
            key: 'id',
            path: 'id',
            label: '#',
            content: product => <span>{product.id}</span>
        },
        {
            key: 'image',
            label: 'Paveikslėlis',
            content: product => <img src={itemImg} alt={product} />
        },

        {
            key: 'title',
            path: 'title',
            label: 'Pavadinimas',
            content: product => <Link to={`/admin/${product.id}`}>{product.title}</Link>
        },
        {
            key: 'quantity',
            path: 'quantity',
            label: 'Kiekis',
            content: product => <span>{product.quantity}</span>
        },
        {
            key: 'delete',
            content: product => <button onClick={() => this.props.onDelete(product)} className="btn btn-danger btn-sm">Ištrinti</button>
        }
    ]


    render() {
        const { products } = this.props;

        return (
            <Table
                columns={this.columns}
                data={products}

            />
        );
    }
}


export default AdminTable;