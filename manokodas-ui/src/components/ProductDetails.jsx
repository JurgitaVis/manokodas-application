import React from 'react';
import { Link } from 'react-router-dom';
import img from '../img/samsung-big.jpg';



const ProductDetails = (props) => {
    console.log(props);

    const { title, description, price, quantity } = props.product;

    return (
        <div >
            <div className="media pt-3">
                <img className="align-self-start mr-3" src={img} alt={title} />
                <div className="media-body">
                    <h5 className="mt-0">{title}</h5>
                    <p>{description}</p>
                    <p>Kiekis: {quantity}</p>
                    <p>Kaina: {price} Eur</p>

                </div>
            </div>

            <div className='row py-5'>
                <button
                    onClick={() => props.onAddToCart(props.product)}
                    className="btn btn-primary ml-3 mr-2">
                    Į krepšelį
                    </button>
                <Link to={'/'} className="btn btn-outline-dark">Atgal</Link>
            </div>

        </div>

    )
}

export default ProductDetails;