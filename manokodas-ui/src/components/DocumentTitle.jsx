import React, { useState } from 'react';
import UseDocumentTitle from '../services/UseDocumentTitle';


export default function DocumentTitle() {

    const [docTitle, setTitle] = useState("e-parduotuvė");

    UseDocumentTitle(`${docTitle}`);

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(e);
        setTitle(e.target.docTitle.value);
    };

    return (
        <form onSubmit={e => { handleSubmit(e) }}>
            <div className="form-row pb-3" >
                <div className="col-auto">
                    <input
                        id="docTitle"
                        name="docTitle"
                        className="form-control"
                    />
                </div>
                <div className="col-auto">
                    <button
                        type="submit"
                        className="btn btn-primary">
                        Pakeisti dokumento pavadinimą
                    </button>

                </div>
            </div>
        </form>

    )
}

