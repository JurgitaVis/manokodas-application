import React, { Component } from 'react';
import CartComponent from '../components/CartComponent';

import apiEndpoint from '../configuration';
import httpService from '../services/httpService';
import UserContext from '../context/UserContext';

class Cart extends Component {
    state = {
        products: [],
        quantity: 0
    }

    componentDidMount() {
        const { userService } = this.context;
        const currentUser = userService.getCurrentUser();

        if (currentUser === "") return;

        httpService
            .get(`${apiEndpoint}/api/users/${currentUser}/cart-products`)
            .then((response) => {
                console.log(response);
                this.setState({ products: response.data });
                userService.setProductCount(response.data.length);
                userService.updateProductCount();
            });
    }


    handleDelete = (id) => {
        const { userService } = this.context;
        const currentUser = userService.getCurrentUser();

        httpService
            .delete(`${apiEndpoint}/api/users/${currentUser}/cart-products/${id}`)
            .then((response) => {
                console.log(response);
                this.setState({ products: response.data });
                userService.setProductCount(response.data.length);
                userService.updateProductCount();
                console.log(userService.getProductCount());
            });

    }

    handleDecrease = (quantity) => {

        if (quantity > 0) {
            quantity--;
        }


        console.log(quantity);

        // httpService
        //     .delete(`${apiEndpoint}/api/users/${currentUser}/cart-products/${id}`)
        //     .then((response) => {
        //         console.log(response);
        //         this.setState({ products: response.data });
        //         userService.setProductCount(response.data.length);
        //         userService.updateProductCount();
        //         console.log(userService.getProductCount());
        //     });

    }
    handleIncrease = (quantity) => {


        quantity++;

        console.log(quantity);


        // httpService
        //     .delete(`${apiEndpoint}/api/users/${currentUser}/cart-products/${id}`)
        //     .then((response) => {
        //         console.log(response);
        //         this.setState({ products: response.data });
        //         userService.setProductCount(response.data.length);
        //         userService.updateProductCount();
        //         console.log(userService.getProductCount());
        //     });

    }

    render() {

        const { length: count } = this.state.products;

        if (count === 0) return <p className="ml-2">Krepšelis tuščias!</p>

        return (
            <div className="container">

                <CartComponent
                    products={this.state.products}
                    onDelete={this.handleDelete}
                    onDecrease={this.handleDecrease}
                    onIncrease={this.handleIncrease}
                />
            </div>
        )
    }
}

Cart.contextType = UserContext;

export default Cart;