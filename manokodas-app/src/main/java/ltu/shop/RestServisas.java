package ltu.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ltu.shop.product.Product;

@RestController
public class RestServisas {

	@Autowired
	List<Product> products;

	@RequestMapping("/prod")
	public List<Product> getProductsCollection() {
		return products;
	}

}
