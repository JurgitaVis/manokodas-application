package ltu.shop.product;

import java.math.BigDecimal;

public class ProductFromService {

	private Long id;
	private String title;
	private String type;
	private String description;
	private String image;
	private BigDecimal price;
	private int quantity;

	public ProductFromService() {
		super();
	}

	public ProductFromService(String title, String type, String description, String image, BigDecimal price,
			int quantity) {
		super();
		this.title = title;
		this.type = type;
		this.description = description;
		this.image = image;
		this.price = price;
		this.quantity = quantity;
	}

	public ProductFromService(Long id, String title, String type, String description, String image, BigDecimal price,
			int quantity) {
		super();
		this.id = id;
		this.title = title;
		this.type = type;
		this.description = description;
		this.image = image;
		this.price = price;
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
