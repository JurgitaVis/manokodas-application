import React from 'react';
import Joi from 'joi-browser';

import apiEndpoint from '../configuration';
import httpService from '../services/httpService';
import Form from '../common/Form';
import DocumentTitle from '../components/DocumentTitle';

class AdminForm extends Form {
    state = {
        data: { title: "", type: "", image: "", description: "", price: "", quantity: "" },
        selectOptions: ["/samsung.jpg"],
        selectTypeOptions: ["Telefonas", "Kita"],
        errors: {},
        productId: ""
    };

    schema = {
        id: Joi.string(),
        title: Joi.string().required().label('Title'),
        type: Joi.string().required().label('Type'),
        image: Joi.string().allow('').optional(),
        description: Joi.string().allow('').optional(),
        price: Joi.number().required().label('Price'),
        quantity: Joi.number().required().label('Quantity')
    }

    componentDidMount() {
        const productId = this.props.match.params.id;
        this.setState({ productId: productId });

        if (productId === "new") return;

        httpService.get(`${apiEndpoint}/api/products/${productId}`)
            .then((response) => {
                this.setState({ data: this.mapToViewModel(response.data) });
            });

    }

    mapToViewModel(product) {
        return {
            title: product.title,
            type: product.type,
            image: product.image,
            description: product.description,
            price: product.price,
            quantity: product.quantity
        }
    };

    doSubmit = () => {

        if (this.state.productId === "new") {
            httpService.post(`${apiEndpoint}/api/products`, this.state.data);
            //console.log(this.state.data);

        } else {
            httpService.put(`${apiEndpoint}/api/products/${this.state.productId}`, this.state.data);
            //console.log(this.state.data);
        }

        this.setState({ data: { title: "", type: "", image: "", description: "", price: "", quantity: "" } });

        this.props.history.push("/admin");
    }

    render() {

        return (
            <div >

                <DocumentTitle />

                <h5 className="pb-3">Administratoriaus Forma</h5>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput('title', 'Pavadinimas')}
                    {this.renderSelect('type', 'Produkto tipas', this.state.selectTypeOptions)}
                    {this.renderSelect('image', 'Paveikslėlio url', this.state.selectOptions)}
                    {this.renderInput('description', 'Aprašymas')}
                    {this.renderInput('price', 'Kaina', 'number')}
                    {this.renderInput('quantity', 'Kiekis', 'number')}
                    {this.renderButton('Save')}
                </form>

            </div>

        );
    }
}

export default AdminForm;