import React from 'react';


export default function LoginComponent({ onLog, currentUser }) {

    return (

        <form className="form-inline" onSubmit={onLog}  >
            {currentUser === "" && <input className="form-control" type="text" name="username" placeholder="Vartotojo vardas" />}
            {currentUser !== "" && <p className="navbar-text form-control my-1">Vartotojas: {currentUser}</p>}

            <button className="btn btn-outline-primary m-1" type="submit">{currentUser === "" ? "Prisijungti" : "Atsijungti"} </button>
        </form>

    )
}

