import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router';
import LoginComponent from '../components/LoginComponent';
import UserContext from '../context/UserContext';


export default function Login() {

    const { userService } = useContext(UserContext);
    const history = useHistory();

    const [currentUser, setCurrentUser] = useState(userService.getCurrentUser());
    userService.updateUser = () => setCurrentUser(userService.getCurrentUser());

    const handleLoginChange = e => {
        e.preventDefault();
        // console.log(e.target.username.value);
        if (currentUser === "") {
            userService.setCurrentUser(e.target.username.value);
            history.push('/cart');
        } else {
            userService.setCurrentUser("");
            userService.setProductCount(0);
            history.push('/');
        }
        userService.updateProductCount();
        userService.updateUser();
        e.target.reset();

    }

    return (
        <LoginComponent currentUser={currentUser} onLog={handleLoginChange} />
    )
}


