package ltu.shop;

import java.math.BigDecimal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import ltu.shop.product.Phone;
import ltu.shop.product.Product;

@Configuration
@ImportResource({ "classpath*:application-context.xml" })
public class AppConfig {

	@Bean
	public Product getProduct1() {
		return new Phone("Samsung 4", "Telefonas", "desc", "/samsung.jpg", new BigDecimal(250), 1);
	}

	@Bean
	public Product getProduct2() {
		return new Phone("Samsung 5", "Telefonas", "desc", "/samsung.jpg", new BigDecimal(200), 2);
	}

	@Bean
	public Product getProduct3() {
		return new Phone("Samsung 6", "Telefonas", "desc", "/samsung.jpg", new BigDecimal(150), 3);
	}

}