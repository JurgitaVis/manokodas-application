package ltu.shop.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ltu.shop.product.Product;
import ltu.shop.product.ProductDAO;
import ltu.shop.product.ProductFromService;

@Service
public class CartService {

	@Autowired
	private CartDAO cartDao;
	@Autowired
	private ProductDAO productDao;

	public CartDAO getCartDao() {
		return cartDao;
	}

	public void setCartDao(CartDAO cartDao) {
		this.cartDao = cartDao;
	}

	public ProductDAO getProductDao() {
		return productDao;
	}

	public void setProductDao(ProductDAO productDao) {
		this.productDao = productDao;
	}

	public List<ProductFromService> addToCart(String username, Long id) {
		Product product = productDao.getOne(id);
		Cart cart = cartDao.findByUsername(username);
		if (cart == null) {
			cart = cartDao.save(new Cart(username));

		}
		cart.addProductToCart(product);
		cartDao.save(cart);

		return cart.getProductsInCart().stream()
				.map(item -> new ProductFromService(item.getId(), item.getTitle(), item.getType(),
						item.getDescription(), item.getImage(), item.getPrice(), item.getQuantity()))
				.collect(Collectors.toList());
	}

	public List<ProductFromService> getProductsInCart(String username) {
		Cart cart = cartDao.findByUsername(username);
		if (cart == null) {
			return new ArrayList<>();
		}
		return cart.getProductsInCart().stream()
				.map(item -> new ProductFromService(item.getId(), item.getTitle(), item.getType(),
						item.getDescription(), item.getImage(), item.getPrice(), item.getQuantity()))
				.collect(Collectors.toList());
	}

	public List<ProductFromService> deleteFromCart(String username, Long id) {
		Cart cart = cartDao.findByUsername(username);
		Product product = productDao.getOne(id);

		cart.removeProductFromCart(product);
		cartDao.save(cart);

		return cart.getProductsInCart().stream()
				.map(item -> new ProductFromService(item.getId(), item.getTitle(), item.getType(),
						item.getDescription(), item.getImage(), item.getPrice(), item.getQuantity()))
				.collect(Collectors.toList());
	}

}
