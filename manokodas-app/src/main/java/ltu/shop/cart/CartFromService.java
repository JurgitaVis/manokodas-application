package ltu.shop.cart;

import java.util.List;

import ltu.shop.product.ProductFromService;

public class CartFromService {

	private String username;

	private List<ProductFromService> productsInCart;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<ProductFromService> getProductsInCart() {
		return productsInCart;
	}

	public void setProductsInCart(List<ProductFromService> productsInCart) {
		this.productsInCart = productsInCart;
	}

}
