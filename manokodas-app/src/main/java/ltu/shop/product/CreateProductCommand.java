package ltu.shop.product;

import java.math.BigDecimal;

public class CreateProductCommand {

	private String title;
	private String description;
	private String image;
	private BigDecimal price;
	private int quantity;

	public CreateProductCommand(String title, String description, String image, BigDecimal price, int quantity) {
		super();
		this.title = title;
		this.description = description;
		this.image = image;
		this.price = price;
		this.quantity = quantity;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
