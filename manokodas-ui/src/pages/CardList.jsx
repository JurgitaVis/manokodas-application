import React, { Component } from 'react';

import apiEndpoint from '../configuration';
import httpService from '../services/httpService';

import ProductCard from '../components/ProductCard';


class CardList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
        };
    }

    componentDidMount() {
        httpService.get(`${apiEndpoint}/api/products`)
            .then((products) => {
                this.setState({ products: products });
            }).catch(error => {
                //return this.props.history.replace("/not-found");
            });
    }


    render() {
        const { data } = this.state.products;

        if (data) {
            return (

                <div className="row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-3 ">
                    {data.map(product => {
                        return <ProductCard key={product.id} id={product.id} product={product} />;
                    })}

                </div>

            );

        } else {
            return (
                <p className="ml-2">Bandom įkelti duomenis...</p>
            )
        }

    }
}


export default CardList;