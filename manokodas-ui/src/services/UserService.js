class UserService {
    constructor() {
        this._username = "";
        this._productCount = 0;
    }

    getCurrentUser = () => {
        return this._username;
    }

    setCurrentUser = (username) => {
        this._username = username;

    }

    getProductCount = () => {
        return this._productCount;
    }

    setProductCount = (count) => {
        this._productCount = count;
    }

    updateUser = () => { }
    updateProductCount = () => { }

}

export default UserService