import React from 'react';

import '../index.css';

import itemImg from "../img/samsung-big.jpg";
import { Link } from 'react-router-dom';


function Card(props) {
    const { id, title, description, price, quantity } = props.product;

    return (
        <div className="col d-flex justify-content-center pb-2">
            <div className="card" >
                <div className="text-center">
                    <img src={itemImg} className="card-img-top pt-1" alt={title} />
                </div>
                <div className="card-body">
                    <h5 className="card-title">{title}</h5>
                    <p className="card-text">{description}</p>
                    <div className="row">
                        <div className="col-6">
                            <p>Kaina {price}</p>
                        </div>
                        <div className="col-6">
                            <p>Kiekis {quantity}</p>
                        </div>
                    </div>
                    <Link to={`product/${id}`} className="btn btn-primary">Peržiūrėti</Link>
                </div>
            </div>

        </div>

    );

}

export default Card;