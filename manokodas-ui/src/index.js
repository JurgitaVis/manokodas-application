import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import "bootstrap/dist/js/bootstrap.bundle.min";

import './index.css';
import App from './App';

import UserContext from './context/UserContext'
import UserService from './services/UserService';


document.title = "E-parduotuvė";
const userService = new UserService();


ReactDOM.render(

  <React.StrictMode>
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <UserContext.Provider value={{ userService }}>
        <App />
      </UserContext.Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
