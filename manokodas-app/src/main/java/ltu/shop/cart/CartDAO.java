package ltu.shop.cart;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CartDAO extends JpaRepository<Cart, Long> {
	Cart findByUsername(String username);
}
