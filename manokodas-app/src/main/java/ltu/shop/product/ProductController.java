package ltu.shop.product;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "products")
@RequestMapping(value = "/api/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get product list", notes = "Returns all products")
	public List<ProductFromService> getProducts(@RequestParam(required = false, value = "title") String title) {
		if (title == null || title == "") {
			return productService.getProducts();
		}
		return productService.getProductsByTitle(title);

	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create product", notes = "Creates new product")
	public void addProduct(@RequestBody ProductFromService cmd) {
		// int id = (getProducts().size() + 1);
		String title = cmd.getTitle();
		String type = cmd.getType();
		String description = cmd.getDescription();
		String image = cmd.getImage();
		BigDecimal price = cmd.getPrice();
		int quantity = cmd.getQuantity();

		productService.addProduct(new ProductFromService(title, type, description, image, price, quantity));
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete product", notes = "Deletes product with specified id")
	public void deleteProduct(@PathVariable final Long id) {
		productService.deleteProduct(id);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "Get product", notes = "Returns product with specified id")
	public ProductFromService getProduct(@PathVariable final Long id) {
		return productService.getProduct(id);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ApiOperation(value = "Update product", notes = "Updates product with specified id")
	public void updateProduct(@PathVariable Long id, @RequestBody ProductFromService cmd) {

		String title = cmd.getTitle();
		String type = cmd.getType();
		String description = cmd.getDescription();
		String image = cmd.getImage();
		BigDecimal price = cmd.getPrice();
		int quantity = cmd.getQuantity();

		productService.updateProduct(new ProductFromService(id, title, type, description, image, price, quantity));
	}

}
