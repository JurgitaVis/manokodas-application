package ltu.shop.user;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

	@Autowired
	private UserDAO userDao;

	@Autowired
	private UserDetailsDAO userDetailsDao;

	@Transactional(readOnly = true)
	public List<UserFromService> getUsers() {
		return userDao.findAll().stream()
				.map(user -> new UserFromService(user.getUsername(), user.getUserDetails().getFirstName(),
						user.getUserDetails().getLastName(), user.getUserDetails().getEmail()))
				.collect(Collectors.toList());
	}

	@Transactional
	public void createUser(UserFromService user) {
		String username = user.getUsername();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEmail();

		UserDetails details = new UserDetails(firstName, lastName, email);

		userDetailsDao.save(details);

		User userToDB = new User(username);
		userToDB.setUserDetails(details);

		userDao.save(userToDB);
	}

	@Transactional
	public void deleteUser(String username) {
		userDao.deleteByUsername(username);
	}

	public UserDAO getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}

	public UserDetailsDAO getUserDetailsDao() {
		return userDetailsDao;
	}

	public void setUserDetailsDao(UserDetailsDAO userDetailsDao) {
		this.userDetailsDao = userDetailsDao;
	}

}
