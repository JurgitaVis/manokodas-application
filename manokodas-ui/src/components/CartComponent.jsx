import React, { Component } from 'react';


import itemImg from '../img/samsung-small.jpg';
import Table from '../common/Table';

class CartComponent extends Component {

    columns = [

        {
            key: 'image',
            label: 'Paveikslėlis',
            content: product => <img src={itemImg} alt={product} />
        },

        {
            key: 'title',
            path: 'title',
            label: 'Pavadinimas',
            content: product => <span>{product.title}</span>
        },
        {
            key: 'quantity',
            path: 'quantity',
            label: 'Kiekis',
            content: product => <div>
                <button onClick={() => this.props.onDecrease(product.quantity)} className="btn btn-primary btn-sm mr-3">- </button>
                {product.quantity}
                <button onClick={() => this.props.onIncrease(product.quantity)} className="btn btn-primary btn-sm ml-3"> +</button>
            </div>
        },
        {
            key: 'delete',
            content: product => <button onClick={() => this.props.onDelete(product.id)} className="btn btn-danger btn-sm">Ištrinti</button>
        }
    ]


    render() {
        const { products } = this.props;

        return (
            <Table
                columns={this.columns}
                data={products}

            />
        );
    }

    // render() {
    //     const { products, onDelete } = this.props;

    //     return (
    //         <table className='table'>
    //             <thead>
    //                 <tr>
    //                     <th scope='col'>Paveikslėlis</th>
    //                     <th scope='col'>Pavadinimas</th>
    //                     <th scope='col'>Kiekis</th>
    //                     <th scope='col'></th>
    //                 </tr>
    //             </thead>
    //             {products.map(({ id, title, quantity }) => (
    //                 <tbody key={id}>
    //                     <tr>
    //                         <td>
    //                             <img src={itemImg} alt={title} />
    //                         </td>
    //                         <td>{title}</td>
    //                         <td>{quantity}</td>
    //                         <td><button onClick={() => onDelete(id)} className="btn btn-danger btn-sm">Trinti</button></td>
    //                     </tr>
    //                 </tbody>
    //             ))}
    //         </table>
    //     );
    // }
}

export default CartComponent;