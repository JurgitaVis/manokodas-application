package ltu.shop.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductDAO extends JpaRepository<Product, Long> {

	@Query("select p from Product p where p.title like %?1%")
	List<Product> findByTitleFragment(String title);

	@Query("select p from Product p order by p.title asc")
	List<Product> findAll();

}
