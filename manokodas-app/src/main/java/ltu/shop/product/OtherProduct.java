package ltu.shop.product;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Kita")
public class OtherProduct extends Product {
	private String type = "Kita";

	public String getType() {
		return type;
	}
}
