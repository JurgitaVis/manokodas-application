package ltu.shop.product;

import java.math.BigDecimal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Telefonas")
public class Phone extends Product {

	private String type = "Telefonas";

	public String getType() {
		return type;
	}

	public Phone() {
		super();

	}

	public Phone(String title, String type, String description, String image, BigDecimal price, int quantity) {
		super(title, description, image, price, quantity);
		this.type = type;
	}

}
