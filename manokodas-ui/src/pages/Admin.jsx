import React, { Component } from 'react';


import apiEndpoint from '../configuration';
import httpService from '../services/httpService';

import { Link } from 'react-router-dom';

import AdminTable from '../components/AdminTable';
import SearchBox from '../common/SeachBox';

export default class Admin extends Component {
    state = {
        products: [],
        searchQuery: ''
    }

    componentDidMount() {
        httpService
            .get(`${apiEndpoint}/api/products`)
            .then((response) => {
                this.setState({ products: response.data });
                console.log(response);
            })
            .catch((err) => {
                console.log(err);
            });
    }


    handleDelete = (item) => {
        const id = item.id;

        httpService.delete(`${apiEndpoint}/api/products/${id}`).then(() => {
            const filtered = this.state.products.filter(itm => itm.id !== item.id);
            this.setState({ products: filtered });
        });

    }

    handleSearch = (e) => {
        const title = e.currentTarget.value;

        this.setState({ searchQuery: title });

        httpService.get(`${apiEndpoint}/api/products?title=${title}`).then((response) => {
            // const filtered = this.state.products.filter(itm => itm.title !== title);
            this.setState({ products: response.data });
        });
    }


    render() {

        const { length: totalCount } = this.state.products;

        // if (totalCount === 0) return <Link to="admin/new" className="btn btn-primary my-2">Pridėti naują</Link>


        return (
            <div>

                <Link to="admin/new" className="btn btn-primary my-2">Pridėti naują</Link>
                <p >Rasta produktų: {totalCount}</p>
                <SearchBox value={this.state.searchQuery} onChange={this.handleSearch} />

                <AdminTable
                    products={this.state.products}
                    onDelete={this.handleDelete}
                />

            </div>

        )
    }
}
