import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import './index.css';
import NavBar from './components/Navigation';
import CardList from './pages/CardList';
import Admin from './pages/Admin';
import Cart from './pages/Cart';
import AboutProduct from './pages/AboutProduct';
import AdminForm from './pages/AdminForm';
import NotFound from './components/NotFound';



class App extends Component {

  render() {
    return (
      <div className="container">

        <NavBar />
        <Switch >
          <Route exact path="/" component={CardList} />
          <Route path="/product/:id" component={AboutProduct} />
          <Route exact path="/admin/:id" component={AdminForm} />
          <Route path="/admin" component={Admin} />
          <Route path="/cart" component={Cart} />
          {/* 
          <Route path="/not-found" component={NotFound}></Route>
          <Redirect to="/not-found" /> */}
          <Route path='*' component={NotFound} />
          <Route component={NotFound} />

        </Switch>

      </div>

    );
  }
}

export default App;
