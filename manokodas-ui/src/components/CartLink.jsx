import React, { useContext, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';

import UserContext from '../context/UserContext';

export default function CartLink() {

    const { userService } = useContext(UserContext);

    const [cartProducts, setProductCount] = useState(userService.getProductCount());
    userService.updateProductCount = () => setProductCount(userService.getProductCount());

    return (
        <div>
            <span> <FontAwesomeIcon icon={faShoppingCart} /> {cartProducts} prekių </span>
        </div>
    )
}
