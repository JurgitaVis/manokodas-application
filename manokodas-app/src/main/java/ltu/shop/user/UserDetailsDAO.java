package ltu.shop.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsDAO extends JpaRepository<UserDetails, Long> {

}
