import React, { Component } from 'react';
import apiEndpoint from '../configuration';
import httpService from '../services/httpService';
import UserContext from '../context/UserContext';

import ProductDetails from '../components/ProductDetails';

class AboutProduct extends Component {

    constructor(props) {
        super(props);
        this.state = {
            product: null
        }
    }

    componentDidMount() {

        httpService
            .get(`${apiEndpoint}/api/products/${this.props.match.params.id}`)
            .then(response => this.setState({ product: response.data }))
            .catch(error => {
                return this.props.history.replace("/");
            });
    }

    handleAddToCart = product => {
        console.log("try to add to cart id:", product.id);
        const { userService } = this.context;
        const currentUser = userService.getCurrentUser();

        if (currentUser === "") return;

        httpService.post(`${apiEndpoint}/api/users/${currentUser}/cart-products`,
            {
                'id': product.id,
                'title': product.title,
                'image': product.image

            }).then(response => {
                console.log("added")
                userService.setProductCount(response.data.length);
                userService.updateProductCount();
                console.log("products in service:", userService.getProductCount());
            }).catch(err => console.log(err));

    }


    render() {
        if (this.state.product !== null) {
            const { product } = this.state;

            return (

                <ProductDetails key={product.id}
                    product={product}
                    onAddToCart={this.handleAddToCart}
                />
            )
        } else {
            return (<p className="ml-2">Bandom įkelti duomenis...</p>)
        }
    }
}

AboutProduct.contextType = UserContext;

export default AboutProduct